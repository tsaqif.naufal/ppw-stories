from django.shortcuts import render
from lab_1.views import mhs_name, birth_date

#TODO Implement
#Create a content paragraph for your landing page:
landing_page_content = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur ac odio accumsan, congue tortor nec, tincidunt felis. Quisque tincidunt justo a scelerisque posuere. Mauris ac tincidunt odio. Nullam ex nunc, pulvinar id dapibus eget, pretium sit amet ipsum. Nunc posuere metus quis mi tincidunt, id molestie lorem ultrices. Pellentesque scelerisque tellus sed libero rhoncus rutrum. Ut metus mi, interdum non dui nec, congue sodales massa. Fusce ornare leo in eros fringilla venenatis.'

def index(request):
    response = {'name': mhs_name, 'content': landing_page_content}
    return render(request, 'index_lab2.html', response)
